# Multilingual QWERTZ #
MQZ (pronounced /mʌks/) is a keyboard layout based on the German QWERTZ and
designed for both typing in many languages using the latin alphabet and
comfortable coding and similar stuff.


## Notable Differeneces form Standard QWERTZ ##
MQZ isn't a superset of the T1 layout of DIN 2137. It uses caps-lock instead of
shift-lock and many special characters as well as ß have been moved. The umlauts
have been removed competely and are instead typed using a dead-key.

