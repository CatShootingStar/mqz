# TODO #


## Diacritical Marks ##
<!--Name (combining Unicode number | dead Xorg character) -->

- [x] grave (U+0300 | 0xfe50)
- [x] double grave (U+030f | 0xfe66)
- [x] acute (U+3001 | 0xfe51)
- [x] double acute (U+030b | 0xfe59)
- [x] circumflex (U+0302 | 0xfe52)
- [x] circumflex below (U+032d | 0xfe69)
- [x] caron (U+030c | 0xfe5a)
- [x] breve (U+0306 | 0xfe55)
- [x] breve below (U+032e | 0xfe6b)
- [x] inverted breve (U+0311 | 0xfe6d)
- [x] tilde (U+0303 | 0xfe53)
- [x] tilde below (U+0330 | 0xfe6a)
- [x] macron (U+0304 | 0xfe54)
- [x] macron below (U+0331 | 0xfe68)
- [x] dot above (U+0307 | 0xfe56)
- [x] dot below (U+0323 | 0xfe60)
- [x] diaresis (U+0308 | 0xfe57)
- [x] diaresis below (U+0324 | 0xfe6c)
- [ ] hook above (U+0309 | 0xfe61)
- [x] ring above (U+030a | 0xfe58)
- [x] ring below (U+0325 | 0xfe67)
- [ ] candrabindu (U+0310 | ?). Not strictly necesarry as it can **probably** be
      represented by *dot above* & *breve*
- [ ] comma above (U+0313 | 0xfe64)
- [ ] reversed comma above (U+0314 | 0xfe65)
- [ ] comma below (U+0326 | 0xfe6e)
- [ ] horn (U+031b | 0xfe62)
- [x] cedilla (U+0327 | 0xfe5b)
- [x] ogonek (U+0328 | 0xfe5c)
- [ ] stroke (U+0335 | 0xfe63)

There are more in the "Combining Diacritical Marks" block. Not all of those
*must* be implemented.


## Characters to debate ##
- ℗ U+2117 Sound recording copyright symbol


## Alternate Layouts ##
This list is incomplete.

- [ ] CJK (Grouped as they have similar challenges in implementation)
  - [ ] Japanese
    - Diacritics:
      - [ ] 濁点 (U+3099 | 0xfe5e)
      - [ ] 半濁点 (U+309a | 0xfe5f)
  - [ ] Korean (Hangul)
  - [ ] Chinese (and Cantonese)
- [ ] Cyrillic
- [ ] Greek
  - Diacritics:
    - [ ] Iota (U+037A | 0xfe5d)
- [ ] Hebrew
- [ ] Thai


## Shipping ##

### Linux ###
- Arch PKGBUILD
- Debian package
- Flatpack packag

